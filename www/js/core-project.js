// Using loose augmentation module pattern, cf. http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
var CORE = (function(my) {

	var reports = CORE.getModule('reportlistAdt');

	var serverLocal = 'http://localhost:8080/Parkeringsinspektorene';	
	var serverDatamagi = 'http://hib.datamagi.no';
	var server = serverLocal;	
	
	my.getReports = function(successCallback, failureCallback) {
		successCallback('http://hib.datamagi.no/reports');
		$.ajax({
			type: 'GET',
			url: server + '/reports', 
			dataType: 'json',
			allowCrossDomainPages: true,
			success: successCallback,
			error: failureCallback
		});
	};


	my.getReport = function(id, successCallback, failureCallback) {
		
		$.ajax({
			type: 'GET',
			url: server + '/reports/' + id, 
			dataType: 'json',
			allowCrossDomainPages: true,
			success: successCallback,
			error: function() {
				console.log('ajax error');
			}
		});
	};
	
	
	
	my.pushReport = function(report, successCallback, failureCallback) {
		var reportJson = JSON.stringify(report);
		console.log(reportJson);
		$.ajax({
			type: 'POST',
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			data: reportJson,
			allowCrossDomainPages: true,
			url: server + '/reports',
			success: function(id) {
				console.log(id);
				//successCallback(reportList);
			},
			error: function(response) {
				if(response.status == 201) {
					successCallback();
					return;
				}
				
				failureCallback();
			}
		});
	};
	
	
	return my;
}(CORE || {}));
