// Using loose augmentation module pattern, cf. http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
var CORE = (function(my) {
	var modules = {}; // or Object.create(null) instead of {} in ES5 to avoid inheriting from Object.prototype

	my.debug = function() { console.log.apply(null, arguments); };

	my.addModule = function(name, module, replaceDuplicate) {
		if (name in modules) {
			if (replaceDuplicate) { my.removeModule(name); }
			else { throw new Error('Module name conflict: ' + name); }
		}
		modules[name] = module;
		if (typeof modules[name].init === 'function') { modules[name].init(); }
	};

	my.removeModule = function(name) {
		if (name in modules) {
			if (typeof modules[name].destroy === 'function') { modules[name].destroy(); }
			delete modules[name];
		}
	};

	// The getModule method is just for internal use / testing of the CORE, it shouldn't be exposed to the modules (i.e. in the facade/sandbox/hub), as that would enable direct module-to-module communication without using the CORE as mediator
	my.getModule = function(name) { return modules[name]; }; // returns undefined if a module with this name has not been added
	my.hasModule = function(name) { return (name in modules); };

	my.broadcast = function(event, data) {
		if (!event) { return; }
		data = data || {};
		my.debug('Mediator received ' + event + ' event');
		
		for (var name in modules) {
			if (typeof modules[name]['on' + event] === 'function') {
				
				try {
					my.debug('Mediator calling on' + event + ' on ' + name);
					modules[name]['on' + event].call(modules[name], data);
				} catch (err) {
					my.debug(['Mediator error:', event, err].join(' '));
				}
			}
		}
	};
	// this version of the CORE mediator uses an implied subscription to events by exposing "on<event>" methods, as opposed to an explicit subscribe method

	// This is a helper function which would normally be in a JS library, cf. http://underscorejs.org/#template
	my.supplant = function(template, data, removeUnmatched) {
		return template.replace(/#{([^{}]*)}/g, function(fullMatch, subMatch) {
			var replacement = data[subMatch];
			return (typeof replacement === 'string' || typeof replacement === 'number') ? replacement : (removeUnmatched === true ? '' : fullMatch);
		});
	},

	my.parseQueryString = function(qs) {
		qs = qs.split('+').join(' ');
		var params = {}, tokens, re = /[?&]?([^=]+)=([^&]*)/g;
		while (tokens = re.exec(qs)) { params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]); }
		return params;
	}

	return my;
}(CORE || {}));
