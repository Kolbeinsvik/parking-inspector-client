CORE.addModule('new', (function() {
	
	var my = {}; // or Object.create(null) instead of {} in ES5 to avoid inheriting from Object.prototype
	
	var submitBtn = document.getElementById("submitButton");
	
	var report = {
		reportedBy: '',
		description: '',
		offender: '',
		latitude: '',
		longitude: ''
	};
	
	var latitudeElem = document.getElementById("geo_lat_input");
	var longitudeElem = document.getElementById("geo_lng_input");
	
	
	var reportSuccessHandler = function(report) {
		window.location = "./index.html";
	};

	var reportFailureHandler = function() {
		var wrapper = document.getElementsByClassName('ReportForm')[0];
		var infoElem = document.createElement('aside');
		infoElem.textContent = 'Error. Could not reach remote server.';
		submitBtn.parentNode.insertBefore(infoElem, submitBtn);
		
		console.log('addReport error');
	};
	
	
	var successGeoCallback = function(geolocation) {
		latitudeElem.value = geolocation.coords.latitude;
		longitudeElem.value = geolocation.coords.longitude;
	};
	
	var failureGeoCallback = function() {
		CORE.pushReport(report, reportSuccessHandler, reportFailureHandler);
	};
	
	
	var addReport = function() {
		//var email = document.getElementById("email_input").value;
		var name = document.getElementById("name_input").value;
		var descriptionInput = document.getElementById("description_input").value;
		var offenderInput = document.getElementById("offender_input").value;
		var latitude = latitudeElem.value;
		var longitude = longitudeElem.value;
		
		
		report.reportedBy = name;
		report.description = descriptionInput;
		report.offender = offenderInput;
		report.latitude = latitude;
		report.longitude = longitude;
		
		
		CORE.pushReport(report, reportSuccessHandler, reportFailureHandler);
	};
	
	
	my.init = function() {
		navigator.geolocation.getCurrentPosition(successGeoCallback, failureGeoCallback,{timeout:10000});
		
		submitBtn.addEventListener("click", addReport, false);
	}
	
	return my;
}()));