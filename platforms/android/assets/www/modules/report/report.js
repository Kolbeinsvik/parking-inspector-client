

CORE.addModule('report', (function() {
	var my = {}; // or Object.create(null) instead of {} in ES5 to avoid inheriting from Object.prototype

	var reportSuccessHandler = function(report) {
		var wrapper = $('.Report');
		var tpl  = $('#Report-contentTpl').html();

		wrapper.append(CORE.supplant(tpl, report));
	};

	var reportFailureHandler = function(report) {
	};

	my.init = function() {
		var id = CORE.parseQueryString(window.location.search.substring(1)).id;
		CORE.getReport(id, reportSuccessHandler, reportFailureHandler);
		console.log('haha');
	};

	my.destroy = function() {
		// ...
	};

	// ...

	return my;
}()));
