
CORE.addModule('reportlistAdt', (function() {
	var my = {}; // or Object.create(null) instead of {} in ES5 to avoid inheriting from Object.prototype
	
	var reports;
	
	
	my.add = function(report) {
		report.id = reports.length + 1;
		reports.push(report);
	};
	
	
	my.get = function(id) {
		for(var i = 0; i < reports.length; i++) {
			var report = reports[i];
			if(report.id == id) {
				return report;
			}
		}
		
		return null;
	};
	
	
	my.getAll = function() {
		return reports;
	};
	
	my.onaddReport = function(report) {
		my.add(report);
	};
	
	my.init = function() {
		reports = new Array();
		reports.push({ 
			id: 1, 
			lat: 60.388070, 
			lng: 5.331850, 
			description: 'I saw something...',
			reporterName: 'Martin Lie', 
			reporterEmail: 'martin@datamagi.no', 
			offender: 'XYZ 12345' 
		});
		reports.push({ id: 2, lat: 60.388070, lng: 5.331850, description: 'I saw something else...', reporterName: 'Per', reporterEmail: 'martin@datamagi.no', offender: 'XYZ 23456' });
		reports.push({ id: 3, lat: 60.388070, lng: 5.331850, description: 'I saw something else...', reporterName: 'Nils', reporterEmail: 'martin@datamagi.no', offender: 'XYZ 23456' });
		reports.push({ id: 4, lat: 60.388070, lng: 5.331850, description: 'I saw something else...', reporterName: 'Petter', reporterEmail: 'martin@datamagi.no', offender: 'XYZ 23456' });
		reports.push({ id: 5, lat: 60.388070, lng: 5.331850, description: 'I saw something else...', reporterName: 'Gunnar', reporterEmail: 'martin@datamagi.no', offender: 'XYZ 23456' });
		reports.push({ id: 6, lat: 60.388070, lng: 5.331850, description: 'I saw something else...', reporterName: 'Heidi', reporterEmail: 'martin@datamagi.no', offender: 'XYZ 23456' });
		reports.push({ id: 7, lat: 60.388070, lng: 5.331850, description: 'I saw something else...', reporterName: 'Else', reporterEmail: 'martin@datamagi.no', offender: 'XYZ 23456' });
		reports.push({ id: 8, lat: 60.388070, lng: 5.331850, description: 'I saw something else...', reporterName: 'Petra', reporterEmail: 'martin@datamagi.no', offender: 'XYZ 23456' });
		reports.push({ id: 9, lat: 60.388070, lng: 5.331850, description: 'I saw something else...', reporterName: 'Reidun', reporterEmail: 'martin@datamagi.no', offender: 'XYZ 23456' });
		reports.push({ id: 10, lat: 60.388070, lng: 5.331850, description: 'I saw something else...', reporterName: 'Roger', reporterEmail: 'martin@datamagi.no', offender: 'XYZ 23456' });
	};

	my.destroy = function() {
	};

	return my;
}()));
	