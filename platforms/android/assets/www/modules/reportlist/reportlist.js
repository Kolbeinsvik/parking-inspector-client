
CORE.addModule('reportlist', (function() {
	var my = {}; // or Object.create(null) instead of {} in ES5 to avoid inheriting from Object.prototype
	
	var list = $('.ReportList');
	var tpl  = $('#ReportList-itemTpl').html();
	
	var reportsSuccessHandler = function(reports) {
		//var reportsPlain = JSON.parse(reportsJson);
		//console.log(reportsJson);
//		var reports = reportsJson;
//		reports.forEach(function(report) {
//			report.id = report.uri.substr(report.uri.lastIndexOf('/') + 1);
//		});
		
		$.each(reports, function(i, report) {
			list.append(CORE.supplant(tpl, report));
		});
		list.listview('refresh');
	};

	var reportsFailureHandler = function(response) {
		list.append('<li>Error. Could not reach remote server. </li>');
		list.listview('refresh');
	};

	
	my.onaddReport = function(report) {
		list.add(CORE.supplant(tpl, report));
	};
	
	my.init = function() {
		CORE.getReports(reportsSuccessHandler, reportsFailureHandler);
		// ...
	};

	my.destroy = function() {
		// ...
	};

	// ...

	return my;
}()));
